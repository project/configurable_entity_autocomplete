<?php

namespace Drupal\configurable_entity_autocomplete\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * A widget that adds more info about referenced and linked entities.
 *
 * @FieldWidget(
 *   id = "entity_autocomplete_configurable",
 *   label = @Translation("Configurable Entity Autocomplete"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityAutocompleteConfigurableWidget extends EntityReferenceAutocompleteWidget {

  const FIELDDEFAULT = "[node:title],[node:type-name],[node:nid]";
  const SEARCHONDEFAULT = "";

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    /*
     * Using the FormElement defined in
     * \Drupal\configurable_entity_autocomplete\Element\EntityAutocompleteConfigurable
     */
    $element['target_id']['#type'] = "entity_autocomplete_configurable";
    $element['target_id']['#label']['#value'] = $this->getSetting('label');
    $element['target_id']['#search_on']['#value'] = $this->getSetting('search_on');
    $element['target_id']['#attached']['library'][] = 'configurable_entity_autocomplete/fix_autocomplete_colors';
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Create the custom setting 'label', and assign the default.
      'label' => self::FIELDDEFAULT,
      'search_on' => self::SEARCHONDEFAULT,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm([], $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => t('Tokens to be used'),
      '#default_value' => ($this->getSetting('label') == NULL ? $this::FIELDDEFAULT : $this->getSetting('label')),
      '#required' => TRUE,
    ];
    // Show the token help relevant to this pattern type.
    $form['pattern_container']['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => 'all',
    ];

    $form['search_on'] = [
      '#type' => 'textfield',
      '#title' => t('Field to search on'),
      '#default_value' => ($this->getSetting('search_on') == NULL ? $this::SEARCHONDEFAULT : $this->getSetting('search_on')),
      '#required' => FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield label: @label', ['@label' => $this->getSetting('label')]);
    $summary[] = t('Perform search on: @search', ['@search' => $this->getSetting('search_on')]);

    return $summary;
  }

}
