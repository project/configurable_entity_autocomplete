<?php

namespace Drupal\configurable_entity_autocomplete;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Utility\Token;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Service to output info about nodes in Autocompleters.
 */
class EntityInfoGetter {

  /**
   * The entity we should get info from.
   *
   * @var Drupal\Core\Entity\Entity
   */
  protected $entity;
  /**
   * The token service.
   *
   * @var Drupal\Core\Utility\Token
   */
  protected $tokenizer;

  /**
   * Constructs the EntityInfoGetter.
   *
   * @var \Drupal\Core\Utility\Token
   */
  public function __construct(Token $tokenizer) {
    $this->tokenizer = $tokenizer;
  }

  /**
   * Sets the node for this object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The node to be used.
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Outputs info about the entity for autocompletes.
   *
   * @return string
   *   The info based on configuration.
   */
  public function getInfo($tokens) {
    $txt = "";
    $tokens = explode(",", $tokens);
    if ($this->entity) {
      $knownType = FALSE;
      if ($this->entity->getEntityType()->id() == 'node') {
        $knownType = "node";
      }
      if ($this->entity->getEntityType()->id() == 'taxonomy_term') {
        $knownType = "term";
      }
      // Using the given entity for replacements.
      if ($knownType !== FALSE) {
        $i = 1;
        foreach ($tokens as $token) {
          $value = $this->tokenizer->replace($token, [$knownType => $this->entity]);
          $value = str_replace(["(", ")"], "", $value);
          $value = strip_tags($value);
          if ($i == count($tokens)) {
            // We assume the entity ID is the last token.
            $value = "(" . trim($value) . ")";
          }
          else {
            $value .= " - ";
          }
          $txt .= $value;
          $i++;
        }
      }
      else {
        // Building a default string as we couldn't use the original string.
        // Removing parenthesis here too.
        $txt = str_replace(["(", ")"], "", $this->entity->label()) . " - (" . $this->entity->id() . ")";
        $txt = $txt . " [" . $this->entity->bundle() . "]";
      }
    }
    if ($this->entity instanceof EntityPublishedInterface) {
      $status = ($this->entity->isPublished()) ? "Published" : "Unpublished";
      $txt .= " [" . $status . "]";
    }
    return $txt;
  }

}
