<?php

namespace Drupal\configurable_entity_autocomplete;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityAutocompleteMatcher;

/**
 * Alters the core EntityAutocompleteMatcher to add more info on nodes.
 */
class AlteredEntityAutocompleteMatcher extends EntityAutocompleteMatcher {

  /**
   * Gets matched labels based on a given search string.
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {

    $matches = [];

    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler' => $selection_handler,
    ];
    $handler = $this->selectionManager->getInstance($options);

    if (isset($string)) {
      // If tokens are mot set then use default autocomplete.
      if (!isset($selection_settings['tokens'])) {
        return EntityAutocompleteMatcher::getMatches($target_type, $selection_handler, $selection_settings, $string);
      }
      // If search_on, then inform the handler.
      // User.
      if ($target_type === 'user') {
        if (!empty($selection_settings['search_on'])) {
          $options['handler'] = 'altered:user';
          $handler = $this->selectionManager->getInstance($options);
        }
      }
      $tokens = $selection_settings['tokens'];

      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 10;
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, $match_limit);

      // Loop through the entities and convert them into autocomplete output.
      foreach ($entity_labels as $values) {
        foreach ($values as $entity_id => $label) {

          $entity = \Drupal::service('entity_type.manager')->getStorage($target_type)->load($entity_id);
          $entity = \Drupal::service('entity.repository')->getTranslationFromContext($entity);;
          $infogetter = \Drupal::service('configurable_entity_autocomplete.get_entity_info');
          $infogetter->setEntity($entity);

          $label = $infogetter->getInfo($tokens);
          $key = $label;
          // Strip things like starting/trailing white spaces, line breaks and
          // tags.
          $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
          // Names containing commas or quotes must be wrapped in quotes.
          $key = Tags::encode($key);
          $matches[] = ['value' => $key, 'label' => $label];
        }
      }
    }

    return $matches;
  }

}
