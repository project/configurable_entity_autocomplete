<?php

namespace Drupal\configurable_entity_autocomplete\Element;


use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Tags;
use Drupal\configurable_entity_autocomplete\Plugin\Field\FieldWidget\EntityAutocompleteConfigurableWidget;

/**
 * Extends EntityAutocomplete allowing us to display more info for references.
 *
 * @FormElement("entity_autocomplete_configurable")
 */
class EntityAutocompleteConfigurable extends EntityAutocomplete {

  /**
   * {@inheritdoc}
   */
  public static function processEntityAutocomplete(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element['#selection_settings']['tokens'] = $element['#label']['#value'];
    $element['#selection_settings']['search_on'] = $element['#search_on']['#value'];
    $element = parent::processEntityAutocomplete($element, $form_state, $complete_form);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    // Process the #default_value property.
    if ($input === FALSE && isset($element['#default_value']) && $element['#process_default_value']) {
      $tokens = EntityAutocompleteConfigurableWidget::FIELDDEFAULT;
      if (isset($element['#label']['#value'])) {
        $tokens = $element['#label']['#value'];
      }
      if (is_array($element['#default_value']) && $element['#tags'] !== TRUE) {
        throw new \InvalidArgumentException('The #default_value property is an array but the form element does not allow multiple values.');
      }
      elseif (!empty($element['#default_value']) && !is_array($element['#default_value'])) {
        // Convert the default value into an array for easier processing in
        // static::getEntityLabels().
        $element['#default_value'] = [$element['#default_value']];
      }

      if ($element['#default_value']) {
        if (!(reset($element['#default_value']) instanceof EntityInterface)) {
          throw new \InvalidArgumentException('The #default_value property has to be an entity object or an array of entity objects.');
        }

        // Extract the labels from the passed-in entity objects, taking access
        // checks into account.
        return static::getConfigurableEntityLabels($element['#default_value'], $tokens);
      }
    }

    // Potentially the #value is set directly, so it contains the 'target_id'
    // array structure instead of a string.
    if ($input !== FALSE && is_array($input)) {
      $entity_ids = array_map(function (array $item) {
        return $item['target_id'];
      }, $input);

      $entities = \Drupal::entityTypeManager()->getStorage($element['#target_type'])->loadMultiple($entity_ids);

      return parent::getEntityLabels($entities);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getConfigurableEntityLabels(array $entities, string $tokens) {
    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository */
    $entity_repository = \Drupal::service('entity.repository');

    $entity_labels = [];
    foreach ($entities as $entity) {
      // Set the entity in the correct language for display.
      $entity = $entity_repository->getTranslationFromContext($entity);

      // Use the special view label, since some entities allow the label to be
      // viewed, even if the entity is not allowed to be viewed.
      $label = ($entity->access('view label')) ? $entity->label() : t('- Restricted access -');

      // Take into account "autocreated" entities.
      if (!$entity->isNew()) {
        $infogetter = \Drupal::service('configurable_entity_autocomplete.get_entity_info');
        $infogetter->setEntity($entity);
        $label = $infogetter->getInfo($tokens);
      }

      // Labels containing commas or quotes must be wrapped in quotes.
      $entity_labels[] = Tags::encode($label);
    }

    return implode(', ', $entity_labels);
  }

}
